#!/bin/bash

#TL=`kpsexpand '$TEXMF'`
TL="/usr/local/texlive/current/texmf-dist"
TL_LOCAL="/usr/local/texlive/texmf-local"

if [ -d "$TL/scripts/xindex" ]
then 
  echo "$TL/scripts/xindex exists"
else
  mkdir -vp $TL/scripts/xindex
fi

if [ -d "$TL_LOCAL/scripts/xindex" ]
then 
  echo "$TL_LOCAL/scripts/xindex exists"
else
  mkdir -vp $TL_LOCAL/scripts/xindex
fi

if [ -d "$TL/tex/lualatex/xindex" ]
then 
  echo "$TL/tex/lualatex/xindex exists"
else
  mkdir -vp $TL/tex/lualatex/xindex
fi

if [ -d "$TL_LOCAL/tex/lualatex/xindex" ]
then 
  echo "$TL_LOCAL/tex/lualatex/xindex exists"
else
  mkdir -vp $TL_LOCAL/tex/lualatex/xindex
fi
echo "copy files ..."

scp ./xindex.lua $TL/scripts/xindex/
scp ./xindex.lua $TL_LOCAL/scripts/xindex/
scp *.lua $TL/tex/lualatex/xindex/
scp *.lua $TL_LOCAL/tex/lualatex/xindex/

#if [ -d "/usr/local/share/lua/5.2" ]
#then
#  scp -r /usr/local/share/lua/5.2/pl $TL/tex/lualatex/xindex/
#else
#  scp -r /usr/local/share/lua/5.3/pl $TL/tex/lualatex/xindex/
#fi

TEXLUA=`which texlua`
BINDIR=`dirname $TEXLUA`
echo "BINDIR=$BINDIR"

if [ -e "$BINDIR/xindex" ] 
then 
  echo "Link exists"
else
  sudo ln -s $TL/scripts/xindex.lua /$BINDIR/xindex
#  sudo ln -s $TL_Local/scripts/xindex.lua /$BINDIR/xindex   # use only the main one
fi
texhash
echo "... done"
